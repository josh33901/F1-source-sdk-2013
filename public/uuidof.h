#ifndef __UUIDOF_INCLUDED
#define __UUIDOF_INCLUDED

#undef _MSC_VER
#include <windows.h>
#define _MSC_VER 1900

#undef PostMessage
#undef PropertySheet
#undef UnlockResource

#ifdef F1_GCC
// nasty hack
#undef __CRT_UUID_DECL
#define __CRT_UUID_DECL(type,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)           \
    extern "C++" {                                                      \
    template<> inline const GUID &__mingw_uuidof<type>() {              \
        static const IID __uuid_inst = {l,w1,w2, {b1,b2,b3,b4,b5,b6,b7,b8}}; \
        return __uuid_inst;                                             \
    }                                                                   \
    template<> inline const GUID &__mingw_uuidof<type*>() {             \
        return __mingw_uuidof<type>();                                  \
    }                                                                   \
    }

#undef __uuidof
#define __uuidof(type) __mingw_uuidof<__typeof(type)>()
#endif
//__CRT_UUID_DECL(IUnknown, 0x00000000, 0x0000, 0x0000, 0xc0,0x00, 0x00,0x00,0x00,0x00,0x00,0x46)
#endif